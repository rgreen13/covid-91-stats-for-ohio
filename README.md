# Playing with Ohio's Covid-19 Data 
This is a small demonstration notebook showing how to do some graphical analysis using COVID-19 data that has been supplied by the State of Ohio. 

## Goals:
1. To have some fun while physically distancing
2. Demonstrate some graphical analysis using Python, Pandas, and Matplotlib
3. Try to reproduce some of Ohio's COVID-19 Dashboard (https://coronavirus.ohio.gov/wps/portal/gov/covid-19/dashboards)
4. Eventually incorporate larger amounts of data

If I'm missing statistics you'd like to see, screwed something up, or you just want to chat, email greenr@bgsu.edu. 